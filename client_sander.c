#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>
#include <signal.h>
#include <pthread.h>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <time.h>
#include "amessage.pb-c.h"

#define BUF_UDP_MAX 255
#define PORT_UDP 32002
#define PORT_TCP 32000

void DieWithError(char *errorMessage);

void send_handler();
void tcp_send();
char* generator_str();

int main(int argc, char const *argv[]) {

	srand(time(NULL));
	send_handler();

}

void send_handler() {
	int sock_udp;
	struct sockaddr_in addr_udp;
	struct sockaddr_in addr_serv;
	char *buf_udp;
	unsigned int addr_serv_len;
	const char *start_str = "send me msg!";


	buf_udp = calloc(BUF_UDP_MAX, sizeof(char))	;


	if ((sock_udp = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0) {
		DieWithError("socket() failed");
	}

	memset(&addr_udp, 0, sizeof(addr_udp));
	addr_udp.sin_family = AF_INET;
	addr_udp.sin_addr.s_addr = htonl(INADDR_ANY);
	addr_udp.sin_port   = htons(PORT_UDP);

	if (bind(sock_udp, (struct sockaddr *) &addr_udp, sizeof(addr_udp)) < 0) {
		DieWithError("bind() failed");
	}
	for (;;) {
		addr_serv_len = sizeof(addr_serv);

		if ((recvfrom(sock_udp, buf_udp, BUF_UDP_MAX, 0, (struct sockaddr *) &addr_serv, &addr_serv_len)) < 0) {
			DieWithError("recvfrom() failed");
		}
		printf("%s %d\n", buf_udp, strcmp(buf_udp, start_str));
			tcp_send(addr_serv);
			sleep(rand() % 10);
	}
	free(buf_udp);

}
char* generator_str() {
	int y = 5 + rand() % 15	;
	char alf[] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
	char* str;
	str = calloc(y, sizeof(char));
	for (int i = 0; i < y; ++i) {
		int t = rand() % 25;
		str[i] = alf[t];
	}
	return str;
};

void tcp_send(struct sockaddr_in addr_tcp) {
	int sock_tcp;
	char *buf_str;
	// int *buf_len_time;
	AMessage msg = AMESSAGE__INIT; // AMessage
	void *buf;                     // Buffer to store serialized data
	unsigned len;                  // Length of serialized data

	buf_str = generator_str();

	// buf_len_time = calloc(2, sizeof(int));

	if ((sock_tcp = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
		DieWithError("socket() failed");
	}

	addr_tcp.sin_port = htons(PORT_TCP);

	if (connect(sock_tcp, (struct sockaddr *) &addr_tcp, sizeof(addr_tcp)) < 0) {
		DieWithError("connect() failed");
	}
	// buf_len_time[0] = 100 + rand() % 200;
	// buf_len_time[1] = strlen(buf_str);

	msg.time = 100 + rand() % 200;
	msg.str = buf_str;
	len = amessage__get_packed_size(&msg);
	buf = malloc(len);
	amessage__pack(&msg,buf);
	printf("размер отправленных данных %d\n", len );

	if (send(sock_tcp, &len, sizeof(unsigned), 0) != sizeof(unsigned)) {
		DieWithError("send() failed");
	}

	if (send(sock_tcp, buf,(size_t) len, 0) != len) {
		DieWithError("send() failed");
	}
	free(buf_str);
	free(buf);
	// free(buf_len_time);
	close(sock_tcp);
}