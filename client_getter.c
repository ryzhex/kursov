#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>
#include <signal.h>
#include <pthread.h>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <time.h>
#include "amessage.pb-c.h"

#define BUF_UDP_MAX 255
#define MAX_MSG_SIZE 1024
#define	PORT_UDP 32001
#define	PORT_TCP 32004

void DieWithError(char *errorMessage);

void get_handler();
void tcp_get(struct sockaddr_in addr_tcp);

int main(int argc, char const *argv[]) {

	get_handler();
}

void get_handler() {
	int sock_udp;
	struct sockaddr_in addr_udp;
	struct sockaddr_in addr_serv;
	unsigned int addr_serv_len;
	char *buf_udp;
	const char *start_str = "I have mas!!";

	buf_udp = calloc(BUF_UDP_MAX, sizeof(char))	;
	if ((sock_udp = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0) {
		DieWithError("socket() failed");
	}

	memset(&addr_udp, 0, sizeof(addr_udp));
	addr_udp.sin_family = AF_INET;
	addr_udp.sin_addr.s_addr = htonl(INADDR_ANY);
	addr_udp.sin_port   = htons(PORT_TCP);

	if (bind(sock_udp, (struct sockaddr *) &addr_udp, sizeof(addr_udp)) < 0) {
		DieWithError("bind() failed");
	}
	addr_serv_len = sizeof(addr_serv);
	for (;;) {

		if ((recvfrom(sock_udp, buf_udp, BUF_UDP_MAX, 0, (struct sockaddr *) &addr_serv, &addr_serv_len)) < 0) {
			DieWithError("recvfrom() failed");
		}
		if (strcmp(buf_udp, start_str) == 0) {
			tcp_get(addr_serv);
		}
		sleep(rand() % 6);
		
	}
}
void tcp_get(struct sockaddr_in addr_tcp) {
	srand(time(NULL));
	int sock_tcp;
	// char *buf_str;
	int buf_len;
	AMessage *msg;
	uint8_t buf[MAX_MSG_SIZE];



	if ((sock_tcp = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
		DieWithError("socket() failed");
	}

	addr_tcp.sin_port = htons(PORT_UDP);

	if (connect(sock_tcp, (struct sockaddr *) &addr_tcp, sizeof(addr_tcp)) < 0) {
		DieWithError("connect() failed");
	}


	if ((recv(sock_tcp, &buf_len, (sizeof(unsigned)), 0)) < 0) {
		DieWithError("recv() failed");
	}

	// buf_str = malloc(sizeof(char) * buf_len);

	if ((recv(sock_tcp, buf, (size_t) buf_len, 0)) < 0) {
		DieWithError("recv() failed");
	}

	msg= amessage__unpack(NULL, buf_len, buf);
	if (msg  == NULL) {	// Something failed
		fprintf(stderr, "error unpacking incoming message\n");
	}

	printf("Полученная строка: %s\n", msg->str );
	sleep(1 + rand() % 7);
	close(sock_tcp);

}