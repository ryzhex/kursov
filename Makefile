all:proto client_sander server client_getter 
client_sander:client_sander.c
	gcc -std=gnu99 -Wall DieWithError.c amessage.pb-c.c client_sander.c -o client_sander -lprotobuf-c
server:server.c
	gcc -std=gnu99 -Wall DieWithError.c amessage.pb-c.c server.c -o server -lpthread -lprotobuf-c
client_getter:client_getter.c
	gcc -std=gnu99 -Wall DieWithError.c amessage.pb-c.c client_getter.c -o client_getter -lprotobuf-c
proto:amessage.proto
	protoc-c --c_out=. amessage.proto
clean:
	rm -rf client_sander server client_getter amessage.pb-c.c amessage.pb-c.h