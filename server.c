#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <pthread.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include "amessage.pb-c.h"

#define MAX_MSG_SIZE 1024
#define MSGPERM 0600
#define MAXPENDING 5
#define MAXMSGQ 4
#define	PORT_UDP_SEND 32001
#define	PORT_TCP_SEND 32004
#define PORT_UDP_GET 32002
#define PORT_TCP_GET 32000

const char *BROAD;

int msgqid;
struct message {
	long mtype;
	char *str;
};



void sig_handler(int i) {
	printf ("exit\n");
	msgctl(msgqid, IPC_RMID, NULL);
	exit(EXIT_SUCCESS);
}

void DieWithError(char *errorMessage);
void server_hendler();

void *tcp_order_accept(void *arg);
void *tcp_order_getter(void *arg);
void *udp_order(void *arg);

void *tcp_operator_accept(void *arg);
void *tcp_operator_sender(void *arg);
void *udp_operator(void *arg);



int main(int argc, char const *argv[])
{
	if (argc < 2) {
		fprintf(stderr, "Usage:  %s ip_broadcast\n", argv[0]);
		exit(1);
	}
	BROAD = argv[1];

	server_hendler();

	return 0;
}


void server_hendler() {
	int result;
	struct sigaction sa;
	key_t MSGKEY = ftok("/etc/drirc", 0);
	pthread_t thread;


	if ((msgqid = msgget(MSGKEY, MSGPERM | IPC_CREAT | IPC_EXCL)) < 0) {
		DieWithError("failed to create message queue with msgqid_que");
	}

	if ((result = pthread_create(&thread, NULL, tcp_order_accept, NULL)) != 0) {
		DieWithError("Creating the tcp_order_accept thread");
	}
	if ((result = pthread_create(&thread, NULL, udp_order, NULL)) != 0) {
		DieWithError("Creating the udp_order thread");
	}


	if ((result = pthread_create(&thread, NULL, tcp_operator_accept, NULL)) != 0) {
		DieWithError("Creating the tcp_operator_accept thread");
	}
	if ((result = pthread_create(&thread, NULL, udp_operator, NULL)) != 0) {
		DieWithError("Creating the udp_operator thread");
	}

	sa.sa_handler = sig_handler;
	sigaction(SIGINT, &sa, 0);
	for (;;) {}


}


void *tcp_order_accept(void *arg) {
	int  servSock, clntSock, result;
	unsigned int lenClntAddr;
	struct sockaddr_in ServAddr, ClntAddr;
	pthread_t thread_client;
	int *clnt_g_args;

	pthread_detach(pthread_self());


	if ((servSock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
		DieWithError("socket() failed");
	}

	memset(&ServAddr, 0, sizeof(ServAddr));
	ServAddr.sin_family = AF_INET;
	ServAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	ServAddr.sin_port = htons(PORT_TCP_GET);

	if (bind(servSock, (struct sockaddr *) &ServAddr, sizeof(ServAddr)) < 0) {
		DieWithError("bind() failed");
	}

	if (listen(servSock, MAXPENDING) < 0) {
		DieWithError("listen() failed");
	}
	for (;;) {

		lenClntAddr = sizeof(ClntAddr);
		if ((clntSock = accept(servSock, (struct sockaddr *) &ClntAddr, &lenClntAddr)) < 0)	{
			DieWithError("accept() failed");
		}

		if ((clnt_g_args = malloc(sizeof(int))) == NULL) {
			DieWithError("malloc() failed");
		}

		*clnt_g_args = clntSock;

		if ((result = pthread_create(&thread_client, NULL, tcp_order_getter, clnt_g_args)) != 0) {
			DieWithError("Creating the first thread_client");
		}

	}

}


void *tcp_order_getter(void *arg) {
	int client_Sock = *(int *) arg;
	free(arg);
	// char *buf_str;
	unsigned buf_len;
	struct message order;
	struct msqid_ds *msgq_set;
	AMessage *msg;
	uint8_t buf[MAX_MSG_SIZE];

	pthread_detach(pthread_self());

	// buf_len = malloc(sizeof(unsigned));

	msgq_set = malloc(sizeof(struct msqid_ds));
	msgctl(msgqid, IPC_STAT, msgq_set);

	while (msgq_set->msg_qnum >= MAXMSGQ ) {
		sleep(2);
		printf("очередь заполненна\n");
		msgctl(msgqid, IPC_STAT, msgq_set);
	}

	if ((recv(client_Sock, &buf_len, sizeof(unsigned), 0)) < 0) {
		DieWithError("recv() failed");
	}

	printf("получено сообщение: длина строки %d\n", buf_len );

	// buf_str = malloc(sizeof(char) * msg->b);

	if ((recv(client_Sock, buf,(size_t) buf_len, 0)) < 0) {
		DieWithError("recv() failed");
	}
	msg= amessage__unpack(NULL, buf_len, buf);
	if (msg  == NULL) {	// Something failed
		fprintf(stderr, "error unpacking incoming message\n");
	}

	order.mtype = 1;
	order.str = msg->str;

	if ((msgsnd (msgqid, &order, sizeof(order) - sizeof(long), 0)) < 0) {
		DieWithError("msgsnd() failed");
	}

	printf("получено сообщение: время: %d строка: %s\n", msg->time, msg->str );
	// free(buf_len);

	return NULL;
}

void *udp_order(void *arg) {
	int sock_udp;
	unsigned int udp_str_len;
	struct sockaddr_in addr_udp;
	struct msqid_ds *msgq_set;

	pthread_detach(pthread_self());

	const char *udp_str = "send me msg!";

	msgq_set = malloc(sizeof(struct msqid_ds));
	if ((sock_udp = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0) {
		DieWithError("socket() failed");
	}
	int status = 1;
	setsockopt(sock_udp, SOL_SOCKET, (SO_BROADCAST | SO_REUSEADDR), &status, sizeof(status));
	memset(&addr_udp, 0, sizeof(addr_udp));
	addr_udp.sin_family = AF_INET;
	addr_udp.sin_addr.s_addr = inet_addr(BROAD);
	addr_udp.sin_port   = htons(PORT_UDP_GET);

	udp_str_len = strlen(udp_str);

	for (;;)
	{
		sleep(3);

		msgctl(msgqid, IPC_STAT, msgq_set);

		printf("Проверка перед получением:\n число сообщений в очереди %ld\n", msgq_set->msg_qnum);
		if (msgq_set->msg_qnum < MAXMSGQ ) {

			if (sendto(sock_udp, udp_str, sizeof(char)*udp_str_len, 0, (struct sockaddr *) &addr_udp, sizeof(addr_udp)) != sizeof(char)*udp_str_len) {
				DieWithError("sendto() sent a different number of bytes than expected");
			}
		} else {
			printf("очеред заполненна\n");
		}
	}


}


void *tcp_operator_accept(void *arg) {
	int  servSock, clntSock, result;
	unsigned int lenClntAddr;
	struct sockaddr_in ServAddr, ClntAddr;
	pthread_t thread_client;
	int *clnt_g_args;

	pthread_detach(pthread_self());

	if ((servSock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
		DieWithError("socket() failed");
	}

	memset(&ServAddr, 0, sizeof(ServAddr));
	ServAddr.sin_family = AF_INET;
	ServAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	ServAddr.sin_port = htons(PORT_UDP_SEND);

	if (bind(servSock, (struct sockaddr *) &ServAddr, sizeof(ServAddr)) < 0) {
		DieWithError("bind() failed");
	}

	if (listen(servSock, MAXPENDING) < 0) {
		DieWithError("listen() failed");
	}
	for (;;) {

		lenClntAddr = sizeof(ClntAddr);
		if ((clntSock = accept(servSock, (struct sockaddr *) &ClntAddr, &lenClntAddr)) < 0)	{
			DieWithError("accept() failed");
		}

		if ((clnt_g_args = malloc(sizeof(int))) == NULL) {
			DieWithError("malloc() failed");
		}

		*clnt_g_args = clntSock;

		if ((result = pthread_create(&thread_client, NULL, tcp_operator_sender, clnt_g_args)) != 0) {
			DieWithError("Creating the first thread_client");
		}

	}
	free(clnt_g_args);
}

void *udp_operator(void *arg) {
	int sock_udp;
	unsigned int udp_str_len;
	struct sockaddr_in addr_udp;
	struct msqid_ds *msgq_set;

	pthread_detach(pthread_self());

	const char *udp_str = "I have mas!!";

	msgq_set = malloc(sizeof(struct msqid_ds));
	if ((sock_udp = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0) {
		DieWithError("socket() failed");
	}
	int status = 1;
	setsockopt(sock_udp, SOL_SOCKET, (SO_BROADCAST | SO_REUSEADDR), &status, sizeof(status));
	memset(&addr_udp, 0, sizeof(addr_udp));
	addr_udp.sin_family = AF_INET;
	addr_udp.sin_addr.s_addr = inet_addr(BROAD);
	addr_udp.sin_port   = htons(PORT_TCP_SEND);

	udp_str_len = strlen(udp_str);

	for (;;)
	{
		sleep(1);

		msgctl(msgqid, IPC_STAT, msgq_set);

		printf("Проверка перед отправкой:\n число сообщений в очереди %ld\n", msgq_set->msg_qnum);
		if (msgq_set->msg_qnum > 0 ) {

			if (sendto(sock_udp, udp_str, sizeof(char)*udp_str_len, 0, (struct sockaddr *) &addr_udp, sizeof(addr_udp)) != sizeof(char)*udp_str_len) {
				DieWithError("sendto() sent a different number of bytes than expected");
			}
		} else {
			sleep(1);

		}
	}

}

void *tcp_operator_sender(void *arg) {
	int client_Sock = *(int *) arg;
	// int buf_len;
	free(arg);
	struct message buf_msg;
	AMessage msg = AMESSAGE__INIT; // AMessage
	void *buf;                     // Buffer to store serialized data
	unsigned len;                  // Length of serialized data

	pthread_detach(pthread_self());

	if (msgrcv(msgqid, &buf_msg, sizeof(struct message), 0, 0) < 0) {
		DieWithError("msgsnd() failed");
	}
	msg.str = buf_msg.str;
	len = amessage__get_packed_size(&msg);
	buf = malloc(len);
	amessage__pack(&msg,buf);

	// buf_len = strlen(buf_msg.str);

	if (send(client_Sock, &len, sizeof(int) , 0) != sizeof(int) ) {
		DieWithError("send() failed");
	}

	if (send(client_Sock, buf, (size_t) len, 0) != len) {
		DieWithError("send() failed");
	}
	free(buf);
	free(buf_msg.str);

	return NULL;
}